package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
@RequestMapping("/")
public class DemoController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DemoController.class);
    @GetMapping()
    public String test() {
        LOGGER.info("Hi this is info");
        LOGGER.warn("Hi this is warn");
        LOGGER.error("Hi this is error");
        LOGGER.debug("Hi this is debug");
        LOGGER.trace("Hi sthis is trace");
        return "hello world ... ";
    }

}
